#!/bin/bash

get_last_element_of_path (){
	IFS='/' read -ra path_as_array <<< "$1";
	last_element=${path_as_array[-1]}
}

# Recursively push a directory 
push_directory_content () {

	local ORIGIN=$1
	local DEST=$2
	
	# Push the subdirectory 
	echo $ORIGIN $'\n' "-->" $DEST
	adb push --sync $ORIGIN $DEST

	for f in $ORIGIN/*
	do
		
		get_last_element_of_path $ORIGIN
		local DIR=$last_element
		
		if [[ -f "$f" || -L "$f" ]]; then
			continue 
			# The element is a file or a link 
			#echo $f $'\n' "-->" $DEST/$DIR
			#adb push --sync $f $DEST/$DIR # this is useless 
		elif [[ -d "$f" ]]; then
			# The element is a directory
			push_directory_content $f $DEST/$DIR
		fi
	done
}

#cd ..
#push_directory_content ./mavros_test/catkin_ws/src /home/root/mavros_test/catkin_ws
#exit 

# Try to push everything first
cd ..
adb push --sync ./voxl-mapper /home/root 

# Push individual source folders recursively as this may fail with 
# the previous global push of the base directory. 
push_directory_content ./voxl-mapper/catkin_ws/src /home/root/voxl-mapper/catkin_ws

#cd ./mavros_test/catkin_ws/src
#adb push --sync apriltag/ /home/root/mavros_test/catkin_ws/src/
#adb push --sync apriltag_ros/ /home/root/mavros_test/catkin_ws/src/
#adb push --sync mavros_interface/ /home/root/mavros_test/catkin_ws/src/
#adb push --sync voxblox_package/ /home/root/mavros_test/catkin_ws/src/
#adb push --sync voxl_navigation/ /home/root/mavros_test/catkin_ws/src/

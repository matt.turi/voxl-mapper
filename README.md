# VOXL-MAPPER

## SETUP
- run ./utils/pull_voxblox.sh
- start docker image
- run ./utils/install_deps.sh (dev or stable)
- run ./utils/init_workspace.sh
    - might need ros name changed depending on where its built

## BUILD
- run ./utils/build.sh
    - if build stalls, change -j and -l flags

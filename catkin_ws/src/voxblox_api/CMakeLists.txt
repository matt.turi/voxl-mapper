## TRIAL 1 ###
# cmake_minimum_required(VERSION 3.3)
# project(voxblox_api)

# find_package(catkin_simple REQUIRED)
# catkin_simple(ALL_DEPS_REQUIRED)

# cs_add_executable(mytests src/mytests.cpp)
# include_directories(include)
# target_link_libraries(mytests
#     pthread
#     /usr/lib64/libmodal_pipe.so
#     /usr/lib64/librc_math.so
# 	/usr/lib64/libmodal_json.so
# )

# cs_install()
# cs_export()

### TRIAL 2 ###
# cmake_minimum_required(VERSION 3.3)
# project(voxblox_api)

# find_package(catkin_simple REQUIRED)
# catkin_simple(ALL_DEPS_REQUIRED)

# include_directories(
#   ./include
#   ${catkin_INCLUDE_DIRS}
# )

# add_executable(voxblox_api src/mytests.cpp)
# target_link_libraries(voxblox_api ${catkin_LIBRARIES}
#     pthread
#     /usr/lib64/libmodal_pipe.so
#     /usr/lib64/librc_math.so
# 	/usr/lib64/libmodal_json.so )
# add_dependencies(voxblox_api ${catkin_EXPORTED_TARGETS})



# install(TARGETS
#   voxblox_api
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION "/home/root/"
#  )

#  cs_install()
#  cs_export(INCLUDE_DIRS ${CATKIN_DEVEL_PREFIX})


### TRIAL 3 ###
set(TARGET vb-api)
cmake_minimum_required(VERSION 3.3)
add_executable(${TARGET} src/mytests.cpp)
add_definitions(-std=c++11)

find_package (glog 0.5.0 REQUIRED)
include(FindProtobuf)
find_package(Protobuf REQUIRED)


include_directories(
    ./include
    ../catkin_ws/install_isolated/bin
    ../catkin_ws/install_isolated/include
    ../catkin_ws/install_isolated/lib
    ../catkin_ws/install_isolated/share
    ../catkin_ws/install_isolated/include/eigen3
    ../catkin_ws/install_isolated/include/google/protobuf
    ../catkin_ws/install_isolated/include/glog/
    ../catkin_ws/devel_isolated/glog_catkin/*
    ../catkin_ws/devel_isolated/protobuf_catkin/*
    ../catkin_ws/install_isolated/lib/
    ${PROTOBUF_INCLUDE_DIR}
)

# find_package(glog REQUIRED)
target_link_libraries(${TARGET}
    pthread
    #protobuf
    ${Boost_LIBRARIES}
    #/usr/lib64/libglog.so
    #../catkin_ws/devel_isolated/glog_catkin/libglog.so
    #/usr/lib/x86_64-linux-gnu/libglog.so.0
    #/home/root/catkin_ws/install_isolated/lib/libvoxblox.so
    /usr/lib64/libmodal_pipe.so
    /usr/lib64/librc_math.so
	/usr/lib64/libmodal_json.so
    glog::glog
)
install(
    TARGETS ${TARGET}
    LIBRARY         DESTINATION /usr/lib
    RUNTIME         DESTINATION /usr/bin
    PUBLIC_HEADER   DESTINATION /usr/include
)






# # cmake_minimum_required(VERSION 3.3)
# # project(voxblox_api)

# # find_package(catkin_simple REQUIRED)
# # catkin_simple(ALL_DEPS_REQUIRED)

# # add_definitions(-std=c++11 -Wall -Wextra)

# # #############
# # # LIBRARIES #
# # #############

# # ############
# # # BINARIES #
# # ############

# # # cs_add_library(mylib1  /usr/lib64/librc_math.so)
# # # cs_add_library(mylib2  /usr/lib64/libmodal_pipe.so)
# # # cs_add_library(mylib3  /usr/lib64/libmodal_json.so)
# # # #    ./include


# # add_executable(mytests
# #   src/mytests.cpp
# # )
# # include_directories(
# #    ./include
# # )

# # target_link_libraries(mytests
# #     pthread
# #     /usr/lib64/libmodal_pipe.so
# #     /usr/lib64/librc_math.so
# #  	  /usr/lib64/libmodal_json.so
# # )







# # # ##########
# # # # EXPORT #
# # # ##########
# # cs_install()
# # cs_export()

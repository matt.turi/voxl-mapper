// mpa deps
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <rc_math.h>
#include <math.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>
#include "common.h"
#include <string.h>

// VB deps
#include <voxblox/core/common.h>
#include <voxblox/core/tsdf_map.h>
#include <voxblox/integrator/tsdf_integrator.h>
#include "voxblox/integrator/esdf_integrator.h"
#include "voxblox/core/layer.h"
#include "voxblox/core/voxel.h"
#include "voxblox/io/layer_io.h"
#include "voxblox/utils/layer_utils.h" // not sure if we need to include this utils header yet, just has checking and handling (I think) for the layers

// using namespace voxblox;  // Going to try to avoid this, but for now follow their convention

// Pointcloud pc_curr; // from voxblox namespace, what our Pointcloud needs to be
// const std::vector<Transformation> poses_curr; //from voxblox docs, what our pose tf matrix needs to be
voxblox::Pointcloud ptcloud;
const std::vector<voxblox::Transformation> poses_curr;

// VIO
#define DEG_TO_RAD	(3.14159265358979323846/180.0)
#define RAD_TO_DEG	(180.0/3.14159265358979323846)

#define BODY_WRT_LOCAL_POSE_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_body_wrt_local/")
#define BODY_WRT_FIXED_POSE_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_body_wrt_fixed/")

static char pipe_path2[MODAL_PIPE_MAX_PATH_LEN];

static void _rotation_to_tait_bryan(float R[3][3], float* roll, float* pitch, float* yaw)
{
	*roll  = atan2(R[2][1], R[2][2]);
	*pitch = asin(-R[2][0]);
	*yaw   = atan2(R[1][0], R[0][0]);

	if(fabs((double)*pitch - M_PI_2) < 1.0e-3){
		*roll = 0.0;
		*pitch = atan2(R[1][2], R[0][2]);
	}
	else if(fabs((double)*pitch + M_PI_2) < 1.0e-3) {
		*roll = 0.0;
		*pitch = atan2(-R[1][2], -R[0][2]);
	}
	return;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	//printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);
	printf("timestamp(ms)|");
	printf("     Position (m)     |");
	printf(" Roll Pitch Yaw (deg) |");
	printf("    Velocity (m/s)    |");
	printf(" angular rate (deg/s) |\n");
	//printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// callback for simple helper when data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	pose_vel_6dof_t* d = modal_pose_vel_6dof_validate_pipe_data(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(d == NULL) return;
	if(n_packets<=0) return;

    printf("getting vio data \n");

	// TODO: figure out how to convert this to a voxblox::Transformation 

	const voxblox::Transformation T_G_C;

	// T_G_C struct // 
	// typedef Eigen::Matrix<Scalar, 3, 1> Vector3;
	// typedef Eigen::Matrix<Scalar, 4, 1> Vector4;
	// typedef Eigen::Matrix<Scalar, 6, 1> Vector6;
	// typedef Eigen::Matrix<Scalar, 3, Eigen::Dynamic> Matrix3X;
	// typedef PositionTemplate<Scalar> Position;
	// typedef RotationQuaternionTemplate<Scalar> Rotation;
	// typedef Eigen::Matrix<Scalar, 3, 3> RotationMatrix;
	// typedef Eigen::Matrix<Scalar, 4, 4> TransformationMatrix;

	// T_G_C constructors // 
	// explicit QuatTransformationTemplate(const TransformationMatrix& T);
	// explicit QuatTransformationTemplate(const RotationQuaternionTemplate<Scalar>& q_A_B, const Position& A_t_A_B);
	// explicit QuatTransformationTemplate(const typename Rotation::Implementation& q_A_B, const Position& A_t_A_B);


	// Process of extracting data //
	// loop through each
	// for(int i=0;i<n_packets;i++){

    //     printf("\r");
	//poses_curr = d[0]; //  (operand types are 'const std::vector<kindr::minimal::QuatTransformationTemplate<float> >' and 'pose_vel_6dof_t') ERROR

	// 	// convert rotation to tait-bryan angles in degrees for easier viewing
	// 	float roll, pitch, yaw;
	// 	_rotation_to_tait_bryan(d[i].R_child_to_parent, &roll, &pitch, &yaw);
	// 	roll	*= (float)RAD_TO_DEG;
	// 	pitch	*= (float)RAD_TO_DEG;
	// 	yaw		*= (float)RAD_TO_DEG;

	// 	// print everything in one go.
	// 	printf("%12ld | %6.2f %6.2f %6.2f | %6.1f %6.1f %6.1f | %6.2f %6.2f %6.2f | %6.2f %6.2f %6.2f |",\
	// 	d[i].timestamp_ns/1000000,\
	// 	(double)d[i].T_child_wrt_parent[0],\
	// 	(double)d[i].T_child_wrt_parent[1],\
	// 	(double)d[i].T_child_wrt_parent[2],\
	// 	(double)roll, (double)pitch, (double)yaw,\
	// 	(double)d[i].v_child_wrt_parent[0],\
	// 	(double)d[i].v_child_wrt_parent[1],\
	// 	(double)d[i].v_child_wrt_parent[2],\
	// 	(double)d[i].w_child_wrt_child[0]* RAD_TO_DEG,\
	// 	(double)d[i].w_child_wrt_child[1]* RAD_TO_DEG,\
	// 	(double)d[i].w_child_wrt_child[2]* RAD_TO_DEG);

    //     printf("\n");
	// 	fflush(stdout);
	//}
	return;
}

// POINTCLOUD STUFF
#define TOF_W		224
#define TOF_H		172

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int newline = 0;

static void _pc_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	//printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);

	printf("timestamp(ms)| points |");
	for(int i=0;i<10;i++){
		printf("  point%d  (XYZ)  |", i);
	}
	printf("\n");
	//printf(RESET_FONT);
	return;
}

static void _pc_helper_cb(__attribute__((unused))int ch, point_cloud_metadata_t meta, float* data, __attribute__((unused)) void* context)
{
    printf("getting pc data \n");
	// TODO: figure out how to convert this to a voxblox::Pointcloud

	voxblox::Pointcloud pt_cloud;
	

	// // normally start printing from the beginning of the array
	// int start = 0;

	// // print beginning fo data line
	// if(!newline) printf("\r");
	// printf("%12ld |%7d |", meta.timestamp_ns/1000000, meta.n_points);

	// // only print up to 10 points
	// int n_to_print = meta.n_points;
	// if(n_to_print>10) n_to_print=10;

	// special case for TOF
	// if(meta.n_points == (TOF_W*TOF_H)){
	// 	// set pointer to the middle row, halfway back, and 5 points left
	// 	// this should mean the 10 points printed sweeps the image center
	// 	start = (((TOF_W*TOF_H)/2)-(TOF_W/2)-(5));
	// }
	//int start = (((TOF_W*TOF_H)/2)-(TOF_W/2)-(5));
	//ptcloud = (data); //(operand types are 'voxblox::Pointcloud {aka std::vector<Eigen::Matrix<float, 3, 1>, Eigen::aligned_allocator<Eigen::Matrix<float, 3, 1> > >}' and 'float*' ERROR)
	// // print the data to be printed
	// for(int i=0;i<n_to_print;i++){
	// 	printf("%5.1f %5.1f %5.1f|",(double)(data[((start+i)*3)+0]),\
	// 								(double)(data[((start+i)*3)+1]),\
	// 								(double)(data[((start+i)*3)+2]));
	// }

	// if(newline) printf("\n");
	// fflush(stdout);

	return;
}

static void _pc_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}



/*
* this will give us the distance, just need to add rrt* implementation for points to go through
* also need to update drone position with these calcs
* need a queue for planned points, then just have the drone move to them in order
* replan: can run at each point again as it traverses through them?
* will need to update the integrator each time from the tsdf layer
* if the distance is too small, would have to replan from scratch at current location
*/
double getMapDistance(voxblox::EsdfMap *esdf_map, const Eigen::Vector3d& position, Eigen::Vector3d& gradient, Eigen::Vector3d& target){
	double distance = 0.0;
	// Perform gradient ascent until distance is sufficient 
	const double eta = 0.01; 
	const uint max_steps = 100; 
	uint i = 0; 
	double          _d = distance; 
	Eigen::Vector3d _p = position; 
	Eigen::Vector3d _g = gradient;
	// main loop
	while(_d > 0.0 && _d < MIN_DISTANCE && i < max_steps){ // can use the batch version of getDistance... as well if we want to provide > 1 position
		esdf_map.getDistanceAndGradientAtPosition(_p, bool interpolate, &_d, &_g);
		_p += gradient*eta; 
		i++; 
		target = _p; // Assign the reached pose as target 
	}
	return distance;
}






int main(int argc, char** argv) {
// Previously written stuff for pulling pointcloud data 
	main_running = 1;
    enable_signal_handler();

    pipe_client_set_point_cloud_helper_cb(5, _pc_helper_cb, NULL);
    pipe_client_set_connect_cb(5, _pc_connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _pc_disconnect_cb, NULL);

    // request a new pipe from the server
    if(pipe_client_construct_full_path("tof_pc", pipe_path)<0){
        fprintf(stderr, "ERROR: Invalid pipe name");
    }

    printf("waiting for server at %s\n", pipe_path);
    int ret = pipe_client_init_channel(5, pipe_path, "voxl-inspect-points", \
                    EN_PIPE_CLIENT_POINT_CLOUD_HELPER| EN_PIPE_CLIENT_AUTO_RECONNECT, 1024);

    printf("passed");
    if(ret<0){
        pipe_client_print_error(ret);
        return -1;
    }
    else{
        printf("We connected to tof I think...");
    }
// // Previously written stuff for pulling vio data - pipe_path should be "vvpx4_body_wrt_local/" I THINK
    strcpy(pipe_path2, BODY_WRT_LOCAL_POSE_PATH);
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path2);
    usleep(500000);
	int ret2 = pipe_client_init_channel(0, pipe_path2, "voxl-inspect-pose", \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
										POSE_6DOF_RECOMMENDED_READ_BUF_SIZE);
	if(ret2){
		fprintf(stderr, "ERROR: failed to open pipe %s\n", pipe_path2);
		pipe_client_print_error(ret2);
		fprintf(stderr, "Either it is not enabled or voxl-vision-px4 is not running\n");
		return -1;
	}

	// check for errors trying to connect to the server pipe
	if(ret2<0){
		pipe_client_print_error(ret2);
		printf(ENABLE_WRAP);
		return -1;
	}
  
	// TSDF MAP
	voxblox::TsdfMap::Config tsdf_map_config;
	tsdf_map_config.tsdf_voxel_size = 0;
	tsdf_map_config.tsdf_voxels_per_side = 0;

	voxblox::TsdfMap tsdf_map = voxblox::TsdfMap(&tsdf_map_config);

	// TSDF INTEGRATOR
	voxblox::FastTsdfIntegrator::Config tsdf_int_config;
	tsdf_int_config.default_truncation_distance = 0;
	tsdf_int_config.max_weight = 0;
	tsdf_int_config.voxel_carving_enabled = 0;
	tsdf_int_config.min_ray_length_m = 0;
	tsdf_int_config.max_ray_length_m = 0;
	tsdf_int_config.use_const_weight = 0;
	tsdf_int_config.allow_clear = 0;
	tsdf_int_config.use_weight_dropoff = 0;
	tsdf_int_config.use_sparsity_compensation_factor = 0;
	tsdf_int_config.integrator_threads = 0;
	tsdf_int_config.enable_anti_grazing = 0;
	tsdf_int_config.start_voxel_subsampling_factor = 0;
	tsdf_int_config.max_consecutive_ray_collisions = 0;
	tsdf_int_config.clear_checks_every_n_frames = 0;
	tsdf_int_config.max_integration_time_s = 0;

	voxblox::FastTsdfIntegrator tsdf_int = voxblox::FastTsdfIntegrator(&tsdf_int_config, tsdf_map.getTsdfLayerPtr);

	// ESDF MAP 
	voxblox::EsdfMap::Config esdf_map_config;
	esdf_map_config.esdf_voxl_size = 0;
	esdf_map_config.esdf_voxels_per_side = 0;

	voxblox::EsdfMap esdf_map = voxblox::EsdfMap(&esdf_map_config);

	// ESDF INTEGRATOR
	voxblox::EsdfIntegrator::Config esdf_int_config;
	esdf_int_config.full_euclidean_distance = 0;
	esdf_int_config.max_distance_m = 0;
	esdf_int_config.min_distance_m = 0;
	esdf_int_config.default_distance_m = 0;
	esdf_int_config.min_diff_m = 0;
	esdf_int_config.min_weight = 0;
	esdf_int_config.num_buckets = 0;
	esdf_int_config.multi_queue = 0;
	esdf_int_config.add_occupied_crust = 0;
	esdf_int_config.clear_sphere_radius = 0;
	esdf_int_config.occupied_sphere_radius = 0;

	voxblox::EsdfIntegrator esdf_int = voxblox::EsdfIntegrator(&esdf_int_config, tsdf_map.getTsdfLayerPtr, esdf_map.getEsdfLayerPtr);

	// once we receive valid and interpolated pointcloud and pose data
	tsdf_int.integratePointCloud(voxblox::Transformation &pose, voxblox::Pointcloud &pt_cloud);


	// once we want to do some planning
	esdf_int.updateFromTsdfLayer(bool clear_updated_flag);	
	Eigen::Vector3d position; position.setZero();
    Eigen::Vector3d gradient; gradient.setZero();
    Eigen::Vector3d target;

	// example filling eigen vectors //
	// target << msg->pose.position.x, 
    //             msg->pose.position.y, 
    //             msg->pose.position.z; 
    // double distance = 0.0;
    // position[0] = msg->pose.position.x; 
    // position[1] = msg->pose.position.y; 
    // position[2] = msg->pose.position.z; 

	double distance = getMapDistance(position, gradient, target);





}
    while(main_running) usleep(500000);

    printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();
}





